function clamp(value, min, max){
    return Math.max(min, Math.min(value, max));
}

function degree2Radians(v){
    return v * Math.PI / 180;
}

function radians2Degree(v){
    return v * 180 / Math.PI;
}

function isPowerOf2(value) {
    return (value & (value - 1)) == 0;
}

var RealMath = {
    clamp: clamp,
    degree2Radians: degree2Radians,
    radians2Degree: radians2Degree,
    isPowerOf2: isPowerOf2
}

/**
 * Vector3 just a adapter to glMatrix's vec3
 * it can change to self implement in future
 * 
 * @author Leimon
 */
class Vector3 {
    constructor(x=0, y=0, z=0){
        this.elements = vec3.create();
        this.elements[0] = x;
        this.elements[1] = y;
        this.elements[2] = z;
        this.onchange = () => {};
    }

    set x(x){
        this.elements[0] = x;
        this.onchange();
    }

    get x(){
        return this.elements[0];
    }

    set y(y){
        this.elements[1] = y;
        this.onchange();
    }

    get y(){
        return this.elements[1];
    }

    set z(z){
        this.elements[2] = z;
        this.onchange();
    }

    get z(){
        return this.elements[2];
    }

    get xyz(){
        return this.elements;
    }

    copy(){
        const nvec = new Vector3();
        vec3.copy(nvec.elements, this.elements);
        return nvec;
    }

    static add(a=Vector3, b=Vector3){
        const v = new Vector3();
        vec3.add(v.elements, a.elements, b.elements);
        return v;
    }

    static multiply(a=Vector3, b=Vector3){
        const v = new Vector3();
        vec3.multiply(v.elements, a.elements, b.elements);
        return v;
    }

    static transform(a=Vector3, m=Matrix4){
        const v = new Vector3();
        vec3.transformMat4(v.elements, a.elements, m.elements);
        return v;
    }

    static subtract(a, b){
        const v = new Vector3();
        vec3.subtract(v.elements, a.elements, b.elements);
        return v;
    }
}

class Vector2 {
    constructor(x, y){
        this.elements = vec2.create();
        this.elements[0] = x || 0;
        this.elements[1] = y || 0;
    }

    set x(x){
        this.elements[0] = x;
    }

    get x(){
        return this.elements[0];
    }

    set y(y){
        this.elements[1] = y;
    }

    get y(){
        return this.elements[1];
    }

    static sub(a, b){
        let v = new Vector2();
        vec2.subtract(v.elements, a.elements, b.elements);
        return v;
    }
}

class Vector4 {
    constructor(x, y, z, w){
        this.elements = vec4.create();
        this.elements[0] = x || 0;
        this.elements[1] = y || 0;
        this.elements[2] = z || 0;
        this.elements[3] = w || 0;
    }

    set x(x){
        this.elements[0] = x;
    }

    get x(){
        return this.elements[0];
    }

    set y(y){
        this.elements[1] = y;
    }

    get y(){
        return this.elements[1];
    }

    set z(z){
        this.elements[2] = z;
    }

    get z(){
        return this.elements[2];
    }

    set w(w){
        this.elements[3] = w;
    }

    get w(){
        return this.elements[3];
    }
}

class Euler {
    constructor(x, y, z, order){
        this._x = x || 0;
        this._y = y || 0;
        this._z = z || 0;
        this.order = order || this.DefaultOrder;
        this.onchange = () => {};
    }

    get x(){
        return this._x;
    }

    set x(x){
        this._x = x;
        this.onchange();
    }

    get y(){
        return this._y;
    }

    set y(y){
        this._y = y;
        this.onchange();
    }

    get z(){
        return this._z;
    }

    set z(z){
        this._z = z;
        this.onchange();
    }

    get DefaultOrder(){
        return 'XYZ';
    }

    copy(){
        const e = new Euler();
        e.x = this.x;
        e.y = this.y;
        e.z = this.z;
        e.order = this.order;
        return e;
    }

    static makeFromMatrix(m, order='XYZ'){
        const euler = new Euler();
        euler.order = order;
        const te = m.elements;
		const m11 = te[ 0 ], m12 = te[ 4 ], m13 = te[ 8 ];
		const m21 = te[ 1 ], m22 = te[ 5 ], m23 = te[ 9 ];
        const m31 = te[ 2 ], m32 = te[ 6 ], m33 = te[ 10 ];
        
        if (order === 'XYZ') {
			euler.y = Math.asin(clamp(m13, - 1, 1 ));
			if (Math.abs( m13 ) < 0.99999 ) {
				euler.x = Math.atan2( - m23, m33 );
				euler.z = Math.atan2( - m12, m11 );
			} else {
				euler.x = Math.atan2( m32, m22 );
				euler.z = 0;
			}
        }
        return euler;
    }
}

class Matrix4 {
    constructor(){
        this.elements = mat4.create();
    }

    static makeFromEuler(euler){
        const m = new Matrix4();
        const te = m.elements;
        const x = euler.x, y = euler.y, z = euler.z;
        const a = Math.cos(x), b = Math.sin(x);
		const c = Math.cos(y), d = Math.sin(y);
		const e = Math.cos(z), f = Math.sin(z);

		if(euler.order === 'XYZ') {
			const ae = a * e, af = a * f, be = b * e, bf = b * f;

			te[ 0 ] = c * e;
			te[ 4 ] = -c * f;
			te[ 8 ] = d;

			te[ 1 ] = af + be * d;
			te[ 5 ] = ae - bf * d;
			te[ 9 ] = -b * c;

			te[ 2 ] = bf - ae * d;
			te[ 6 ] = be + af * d;
			te[ 10 ] = a * c;
        }else{
            console.warn('not support euler order ' + euler.order);
        }
        // bottom row
		te[ 3 ] = 0;
		te[ 7 ] = 0;
		te[ 11 ] = 0;

		// last column
		te[ 12 ] = 0;
		te[ 13 ] = 0;
		te[ 14 ] = 0;
        te[ 15 ] = 1;
        
        return m;
    }

    static multiply(a, b){
        const m = new Matrix4();
        mat4.multiply(m.elements, a.elements, b.elements);
        return m;
    }

    static translate(m, v){
        const t = new Matrix4();
        mat4.translate(t.elements, m.elements, v.elements);
        return t;
    }

    static scale(m, v){
        const t = new Matrix4();
        mat4.scale(t.elements, m.elements, v.elements);
        return t;
    }

    static makeFromTranslation(v){
        const t = new Matrix4();
        mat4.fromTranslation(t.elements, v.elements);
        return t;
    }

    static makeFromScaling(v){
        const t = new Matrix4();
        mat4.fromScaling(t.elements, v.elements);
        return t;
    }

    static lookat(eye, center, up){
        const t = new Matrix4();
        mat4.lookAt(t.elements, eye.elements, center.elements, up.elements);
        return t;
    }

    static perspective(fieldOfView, aspect, near, far){
        const m = new Matrix4();
        mat4.perspective(m.elements, fieldOfView, aspect, near, far);
        return m;
    }

    static invert(m){
        const t = new Matrix4();
        mat4.invert(t.elements, m.elements);
        return t;
    }

    static transpose(m){
        const t = new Matrix4();
        mat4.transpose(t.elements, m.elements);
        return t;
    }

    copy(){
        const m = new Matrix4();
        m.elements = mat4.clone(this.elements);
        return m;
    }
}

class Transform {
    constructor(){
        this.position = new Vector3();
        this.rotation = new Euler();
        this.scale = new Vector3(1, 1, 1);
    }

    copy(){
        const nt = new Transform();
        for (const [key, value] of Object.entries(this)) {
            nt[key] = value.copy();
        }
        return nt;
    }
}

class TransformMatrices {
    constructor(){
        this.translate = new Matrix4();
        this.scale = new Matrix4();
        this.rotation = new Matrix4();
    }

    copy(){
        const nt = new Transform();
        for (const [key, value] of Object.entries(this)) {
            nt[key] = value.copy();
        }
        return nt;
    }
}

export {Vector3, Euler, Matrix4, Transform, RealMath, Vector4, Vector2, TransformMatrices};