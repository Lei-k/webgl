import {GLComponent} from '../core/core.js';

class Shader extends GLComponent {
    constructor(source, type){
        super();
        this.source = source;
        this.type = type;
        this.id = this.gl.createShader(type);
    }

    upload(){
        this.gl.shaderSource(this.id, this.source);
    }

    compile(){
        this.gl.compileShader(this.id);
    
        if (!this.gl.getShaderParameter(this.id, this.gl.COMPILE_STATUS)) {
            alert('An error occurred compiling the shaders: ' + this.gl.getShaderInfoLog(this.id));
            this.gl.deleteShader(this.id);
            return false;
        }
        return true;
    }
}

class ShaderProgram extends GLComponent {
    constructor(){
        super();
        this.id = this.gl.createProgram();
        this.shaders = [];
    }

    attach(shader=Shader){
        this.shaders.push(shader);
        this.gl.attachShader(this.id, shader.id)
    }

    link(){
        this.gl.linkProgram(this.id);
        if (!this.gl.getProgramParameter(this.id, this.gl.LINK_STATUS)) {
            alert('Unable to initialize the shader program: ' + this.gl.getProgramInfoLog(this.id));
            return false;
        }
        return true;
    }

    getAttribLocation(name=''){
        return this.gl.getAttribLocation(this.id, name);
    }

    getUniformLocation(name=''){
        return this.gl.getUniformLocation(this.id, name);
    }

    vertexAttribPointer(name='', size=3, type=0, normalized=false, stride=0, offset=0){
        const idx = this.getAttribLocation(name);
        this.gl.vertexAttribPointer(idx, size, type, normalized, stride, offset);
    }

    enableVertexAttrib(name=''){
        const idx = this.getAttribLocation(name);
        this.gl.enableVertexAttribArray(idx);
    }

    use(){
        this.gl.useProgram(this.id);
    }

    uniformMatrix4fv(name='', val, transpose=false){
        const idx = this.getUniformLocation(name);
        this.gl.uniformMatrix4fv(idx, transpose, val);
    }

    getParameter(type){
        return this.gl.getProgramParameter(this.id, type);
    }

    getActiveAttrib(idx){
        return this.gl.getActiveAttrib(this.id, idx);
    }

    uniform1i(name='', val){
        const idx = this.getUniformLocation(name);
        this.gl.uniform1i(idx, val);
    }
}

export {Shader, ShaderProgram};