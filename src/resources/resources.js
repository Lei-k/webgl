
var meshMap = new Map();
var textureMap = new Map();
var materials = new Map();
class Resources {
    static findMesh(name){
        return meshMap.get(name);
    }

    static registerMesh(name, mesh){
        meshMap.set(name, mesh);
    }

    static findTexture(name){
        return textureMap.get(name);
    }

    static registerTexture(name, texture){
        textureMap.set(name, texture);
    }

    static findMaterial(name){
        return materials.get(name);
    }

    static registerMaterial(name, material){
        materials.set(name, material);
    }

    static resourceUrl(url){
        const idx = url.indexOf('resource');
        if(idx < 0){
            let clean = url.replace('../', '');
            clean = clean.replace('./', '');
            return 'resource' + (clean.charAt(0) === '/' ? + '' : '/') + clean;
        }
        return url.substr(idx);
    }
}

export {Resources};