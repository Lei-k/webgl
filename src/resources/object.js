import {GLComponent} from '../core/core.js';

class Vbo extends GLComponent {
    /**
     * @param {*} gl opengl context
     * @param {*} data number array
     */
    constructor(data, target){
        super();
        this.data = data;
        this.id = this.gl.createBuffer();
        this.target = target;
    }

    bind(){
        this.gl.bindBuffer(this.target, this.id);
    }

    upload(usage){
        this.gl.bufferData(this.target, this.data, usage);
    }
}

class Vao extends GLComponent {
    constructor(){
        super();
        this.attributes = {};
    }

    addVbo(name='', vbo){
        if(!name || name === ''){
            console.warn('attribte name can not be an empty value');
        }

        if(!vbo){
            console.warn('attribte vbo can not be an empty value');
        }

        if(name === 'uv'){
            let attribute = this.attributes['uvs'];
            if(attribute){
                attribute.push(vbo);
            }else{
                attribute = [vbo];
            }
            this.attributes['uvs'] = attribute;
        }else{
            this.attributes[name] = vbo;
        }
    }

    bind(){
        for (const [key, attribute] of Object.entries(this.attributes)) {
            if(key === 'uvs'){
                attribute.forEach(function(vbo){
                    vbo.bind();
                })
            }else{
                attribute.bind();
            }
        }
    }

    upload(usage){
        for (const [key, attribute] of Object.entries(this.attributes)) {
            if(key === 'uvs'){
                attribute.forEach(function(vbo){
                    vbo.bind();
                    vbo.upload(usage);
                })
            }else{
                attribute.bind();
                attribute.upload(usage);
            }
        }
    }
}

export {Vbo, Vao}