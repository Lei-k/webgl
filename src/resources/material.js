import { GLComponent } from '../core/core.js';

class Material extends GLComponent {
    constructor(program){
        super();
        this.program = program;
    }

    use(){
        this.program.use();
    }
}

export{Material};