import {GLComponent, RealEvent} from '../core/core.js';
import {RealMath} from '../math/math.js';

class Texture extends GLComponent {

    constructor(){
        super();
        this.id = undefined;
        // this is texture unit mapping to this texture
        this.unit = undefined;
    }

    //
    // Initialize a texture and load an image.
    // When the image finished loading copy it into the texture.
    //
    load(url) {
        this.loaded = false;
        console.log('load... ' + url)
        this.id = this.gl.createTexture();
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.id);
    
        // Because images have to be download over the internet
        // they might take a moment until they are ready.
        // Until then put a sinthis.gle pixel in the texture so we can
        // use it immediately. When the image has finished downloading
        // we'll update the texture with the contents of the image.
        const level = 0;
        const internalFormat = this.gl.RGBA;
        const width = 1;
        const height = 1;
        const border = 0;
        const srcFormat = this.gl.RGBA;
        const srcType = this.gl.UNSIGNED_BYTE;
        const pixel = new Uint8Array([0, 0, 255, 255]);  // opaque blue
        this.gl.texImage2D(this.gl.TEXTURE_2D, level, internalFormat,
                    width, height, border, srcFormat, srcType,
                    pixel);
    
        const image = new Image();
        image.crossOrigin = "Anonymous";
        let that = this;

        image.onload = function() {
            that.gl.bindTexture(that.gl.TEXTURE_2D, that.id);
            that.gl.texImage2D(that.gl.TEXTURE_2D, level, internalFormat,
                            srcFormat, srcType, image);
        
            // WebGL1 has different requirements for power of 2 images
            // vs non power of 2 images so check if the image is a
            // power of 2 in both dimensions.
            if (RealMath.isPowerOf2(image.width) && RealMath.isPowerOf2(image.height)) {
                // Yes, it's a power of 2. Generate mips.
                that.gl.generateMipmap(that.gl.TEXTURE_2D);
            } else {
                // No, it's not a power of 2. Turn of mips and set
                // wrapping to clamp to edge
                that.gl.texParameteri(that.gl.TEXTURE_2D, that.gl.TEXTURE_WRAP_S, that.gl.CLAMP_TO_EDGE);
                that.gl.texParameteri(that.gl.TEXTURE_2D, that.gl.TEXTURE_WRAP_T, that.gl.CLAMP_TO_EDGE);
                that.gl.texParameteri(that.gl.TEXTURE_2D, that.gl.TEXTURE_MIN_FILTER, that.gl.LINEAR);
            }

            that.dispatchEvent(new RealEvent('load'));
            that.loaded = true;
        };
        
        image.src = url;
    }

    createEmpty() {
        this.id = this.gl.createTexture();
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.id);
    
        // Because video has to be download over the internet
        // they might take a moment until it's ready so
        // put a sinthis.gle pixel in the texture so we can
        // use it immediately.
        const level = 0;
        const internalFormat = this.gl.RGBA;
        const width = 1;
        const height = 1;
        const border = 0;
        const srcFormat = this.gl.RGBA;
        const srcType = this.gl.UNSIGNED_BYTE;
        const pixel = new Uint8Array([0, 0, 255, 255]);  // opaque blue
        this.gl.texImage2D(this.gl.TEXTURE_2D, level, internalFormat,
                        width, height, border, srcFormat, srcType,
                        pixel);
    
        // Turn off mips and set  wrapping to clamp to edge so it
        // will work regardless of the dimensions of the video.
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
    }

    updateByVideo(video){
        const level = 0;
        const internalFormat = this.gl.RGBA;
        const srcFormat = this.gl.RGBA;
        const srcType = this.gl.UNSIGNED_BYTE;
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.id);
        this.gl.texImage2D(this.gl.TEXTURE_2D, level, internalFormat,
                    srcFormat, srcType, video);
    }

    /**
     * bind to a specific texture unit
     * default value is zero;
     * @param {*} unit 
     */
    bind(unit=0){
        this.unit = this.gl.TEXTURE0+unit;
        this.gl.activeTexture(this.gl.TEXTURE0+unit);
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.id);
    }
}

export {Texture};