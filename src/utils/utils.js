function extractValusString(valueStr){
    const normalizeStr = valueStr.replace(/[^,0-9\.-]/g, '');
    let values = [];
    let vstr = '';
    let last = normalizeStr;
    let idx = 0;
    do{
        idx = last.indexOf(',')
        if(idx > 0){
            vstr = last.substr(0, idx);
            last = last.substr(idx+1);
        }else{
            vstr = last.substr(0);
        }
        values.push(Number(vstr));
    }while(idx > 0);

    return values;
}

var sdomParser = undefined;
class DOMParserFactory {
    static get DOMParser(){
        if(!sdomParser){
            sdomParser = new DOMParser();
        }
        return sdomParser;
    }
}

export {extractValusString, DOMParserFactory};