export {Vbo, Vao} from './resources/object.js';
export {Vector3, Matrix4, RealMath, Transform, Euler} from './math/math.js';
export {Shader, ShaderProgram} from './resources/shader.js';
export {SceneNode, SceneObject, Skybox, Camera, DirectionalLight, PointLight, SpotLight, SceneManager, PerspectiveCamera} from './scene/scene.js';
export {Texture} from './resources/texture.js';
export {GLContextManager} from './core/contex-manager.js';
export {Input} from './core/input.js';
export {Real} from './core/core.js';

import {backend} from './core/real-backend.js';
import {Input} from './core/input.js'

window.addEventListener('load', function() {
    backend.glMain();
})

window.addEventListener('keydown', function(event){
    Input.setKey(event.key.charCodeAt(0), true);
})

window.addEventListener('keyup', function(event){
    Input.setKey(event.key.charCodeAt(0), false);
})