import {Vertex, VertexBuffers} from '../core/geometry.js';

let v = new Vertex(1, 2, 1, [3, 3]);
let vs = [];
vs.push(v);
v = new Vertex(3, 3, 3, [1, 2]);
vs.push(v);
console.log(vs)
console.log(VertexBuffers.makeFromVertices(vs));