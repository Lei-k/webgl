
import { Vector3, Euler} from '../math/math.js';
import {RealNode} from '../core/core.js';
import {SceneFactory} from '../scene/scene-factory.js';
import {SceneBuilder} from '../scene/scene-builder.js';
import {SDLParser} from './sdl-parser.js';

const sceneBuilder = new SceneBuilder();
var parser;
window.addEventListener('load', () => {
    parser = new SDLParser();
})

class SDLElement extends RealNode {
    constructor(){
        super();
        this.tagName = '';
        this.attributes = new Map();
    }

    attributeProvider(){
        return {id: ''};
    }

    retrieveAttribute(attName){
        let providers = this.attributeProvider();
        if(!providers[attName]) return '';
        return providers[attName];
    }

    getAttribute(name){
        return this.attributes.get(name);
    }

    setAttribute(name, val){
        this.attributes.set(name, val);
    }

    findById(id){
        if(this.id === id) return this;

        let children = this.children;
        for(let i=0; i < children.length; i++){
            let rs = children[i].findById(id);
            if(rs) return rs;
        }
        return undefined;
    }

    querySelector(selector){
        if(selector.charAt(0) === '#'){
            let id = selector.substr(1);
            return this.findById(id);
        }
    }

    appendChild(child){
        super.addChild(child);
        if(this.sceneNode && child.sceneNode){
            this.sceneNode.addChild(child.sceneNode);
        }
    }

    append(value){
        if(!parser){
            console.warn('');
            return;
        }
        const inner = parser.parse(value);
        parser.dom = undefined;
        sceneBuilder.build(inner);
        this.appendChild(inner);
        return inner;
    }
}

class SDLWorldElement extends SDLElement {
    constructor(){
        super();
        this.sceneNode = undefined;
        this._position = undefined;
        this._rotation = undefined;
        this._scale = undefined;
    }

    attributeProvider(){
        let supports = super.attributeProvider();
        supports.position = new Vector3();
        supports.rotation = new Euler();
        supports.scale = new Vector3();
        supports.onupdate = event => {};
        return supports;
    }

    set position(position){
        this._position = position;
        this._position.onchange = () => {
            if(this.sceneNode){
                this.sceneNode.changes.position = true;
            }
        }
        if(this.sceneNode){
            this.sceneNode.transform.position = position;
            this.sceneNode.changes.position = true;
        }
    }

    get position(){
        return this._position;
    }

    set rotation(euler){
        this._rotation = euler;
        this._rotation.onchange = () => {
            if(this.sceneNode){
                this.sceneNode.changes.rotation = true;
            }
        }
        if(this.sceneNode){
            this.sceneNode.transform.rotation = euler;
            this.sceneNode.changes.rotation = true;
        }
    }

    get rotation(){
        return this._rotation;
    }

    set scale(scale){
        this._scale = scale;
        this._scale.onchange = () => {
            if(this.sceneNode){
                this.sceneNode.changes.scale = true;
            }
        }
        if(this.sceneNode){
            this.sceneNode.transform.scale = scale;
            this.sceneNode.changes.scale = true;
        }
    }

    get scale(){
        return this._scale;
    }

    get rotationMatrix(){
        if(this.sceneNode){
            return this.sceneNode.transformMatrices.rotation;
        }
        return undefined;
    }
}

class SDLWorldNodeElement extends SDLWorldElement {
    constructor(){
        super();
    }
}

class SDLModelElement extends SDLWorldElement {
    constructor(){
        super();
    }

    attributeProvider(){
        let supports = super.attributeProvider();
        supports.src = '';
        supports.mesh = '';
        supports.texture = '';
        return supports;
    }

    get mesh(){
        return this._mesh;
    }

    set mesh(mesh){
        const f = this._mesh == mesh;
        this._mesh = mesh;
        if(!this.sceneNode || f) return;
        const factory = SceneFactory.factory(this.tagName);
        const r = factory.parseElementAttr('mesh', mesh);
        this.sceneNode._mesh = r.value;
    }

    get texture(){
        return this._texture;
    }

    set texture(texture){
        const f = this._texture == texture;
        this._texture = texture;
        if(!this.sceneNode || f) return;
        const factory = SceneFactory.factory(this.tagName);
        const r = factory.parseElementAttr('texture', texture);
        if(r.value.loaded){
            this.sceneNode.texture = r.value;
        }else {
            const that = this;
            r.value.addEventListener('load', event => {
                that.sceneNode.texture = r.value;
            })
        }
    }
}

class SDLSkyboxElement extends SDLWorldElement {
    constructor(){
        super();
    }

    attributeProvider(){
        let supports = super.attributeProvider();
        supports.front = '';
        supports.back = '';
        supports.right = '';
        supports.left = '';
        supports.up = '';
        supports.down = '';
        return supports;
    }
}

class SDLCameraElement extends SDLWorldElement {
    constructor(){
        super();
    }

    attributeProvider(){
        let providers = super.attributeProvider();
        providers.type = '';
        return providers;
    }
}

class SDLightElement extends SDLWorldElement {
    constructor(){
        super();
    }

    attributeProvider(){
        let providers = super.attributeProvider();
        providers.type = '';
        providers.direction = new Vector3();
        return providers;
    }
}

export {SDLElement, SDLCameraElement, SDLightElement, SDLModelElement, SDLWorldNodeElement, SDLSkyboxElement, SDLWorldElement}