import { Vector3, Euler, RealMath } from '../math/math.js';
import {extractValusString} from '../utils/utils.js';
import {SDLElement, SDLCameraElement, SDLightElement, SDLModelElement, SDLWorldNodeElement, SDLSkyboxElement, SDLWorldElement} from './sdl-elements.js';

class Stack {
    constructor(){
        this.elements = [];
    }

    push(element){
        this.elements.push(element);
    }

    pop(){
        let idx = this.elements.length-1;
        if(idx < 0) return null;
        let element = this.elements[idx];
        this.elements.splice(idx, 1);
        return element;
    }

    peek(){
        let idx = this.elements.length-1;
        if(idx < 0) return null;
        let element = this.elements[idx];
        return element;
    }
}

var SDLParseState = {
    NEW_TAG: 0,
    NEW_ATTRIBUTE: 1,
    NEW_VALUE: 2,
    END_TAG: 3,
    NONE: 99
}

class SDLParser {
    constructor(){
        this.stack = new Stack();
        this.state = SDLParseState.NEW_TAG;
        this.token = '';
        this.val = '';
        this.dom;
    }

    createSDLElement(token){
        token = token.toUpperCase();
        let element;
        if(token === 'WORLD'){
            element = new SDLWorldElement();
        }else if(token === 'MODEL'){
            element = new SDLModelElement();
        }else if(token === 'CAMERA'){
            element = new SDLCameraElement();
        }else if(token === 'LIGHT'){
            element = new SDLightElement();
        }else if(token === 'SKYBOX'){
            element = new SDLSkyboxElement();
        }else if(token == 'NODE'){
            element = new SDLWorldNodeElement();
        }else {
            element = new SDLElement();
        }
        element.tagName = token;
        // save root document;
        if(!this.dom){
            this.dom = element;
        }
        return element;
    }

    parseAttribute(attName, value, element){
        let v = element.retrieveAttribute(attName);
        let type = typeof v;
        if(v instanceof Vector3){
            const vals = extractValusString(value);
            v.x = vals[0], v.y = vals[1], v.z = vals[2];
        }else if(v instanceof Euler){
            const vals = extractValusString(value);
            let x = RealMath.degree2Radians(vals[0]);
            let y = RealMath.degree2Radians(vals[1]);
            let z = RealMath.degree2Radians(vals[2]);
            v.x = x, v.y = y, v.z = z;
        }else if(type === 'string' || v instanceof String){
            v = value;
        }else if(type === 'function'){
            
        }
        else {
            console.warn('this element don\'t support ' + value);
        }

        if(attName === 'position' || attName === 'scale' || attName === 'rotation'){
            v.onchange = () => {
                if(element.sceneNode){
                    element.sceneNode.changes[attName] = true;
                }
            }
        }

        return v;
    }

    processToken(c, idx, content){
        if(c === '<'){
            this.token = '';
            this.val = '';
            this.state = SDLParseState.NEW_TAG;
        }else if(c === ' ' || c === '>'){
            if(this.state === SDLParseState.NEW_TAG){
                let preElement = this.stack.peek();
                let element = this.createSDLElement(this.token);
                if(preElement){
                    preElement.addChild(element);
                }
                this.stack.push(element);
                this.token = '';
                if(c === ' '){
                    this.state = SDLParseState.NEW_ATTRIBUTE;
                }else {
                    this.state = SDLParseState.NONE;
                }
            }
            
        }else if(c === '/'){
            if(this.state === SDLParseState.NEW_VALUE){
                this.val += c;
                return;
            }
            this.token = '';
            this.val = '';
            this.stack.pop();
            this.state = SDLParseState.NONE;
        }else if(c !== '=' && c !== '"') {
            if(this.state === SDLParseState.NONE){
                this.state = SDLParseState.NEW_ATTRIBUTE;
            }

            if(this.state === SDLParseState.NEW_ATTRIBUTE
            || this.state === SDLParseState.NEW_TAG){
                this.token += c;
            }else if(this.state === SDLParseState.NEW_VALUE){
                this.val += c;
            }else {
                console.warn('parse error');
            }
        }else if(c === '"') {
            if(this.state === SDLParseState.NEW_VALUE){
                let element = this.stack.peek();
                if(element){
                    let value = this.parseAttribute(this.token, this.val, element);
                    if(value){
                        element[this.token] = value;
                        element.attributes.set(this.token, value);
                    }
                }
                this.token = '';
                this.val = '';
                this.state = SDLParseState.NONE;
            }else if(this.state === SDLParseState.NEW_ATTRIBUTE){
                this.state = SDLParseState.NEW_VALUE;
            }
        }
    }

    normailzeSDL(sdl){
        let normalSdl = sdl.replace(/\n/g, '');
        normalSdl = normalSdl.replace(/\r/g, '');
        normalSdl = normalSdl.replace(/>[^<]+</g, function(match){
            return '>' + match.substr(1, match.length-2).replace(/\s/g, '') + '<';
        });
        return normalSdl;
    }

    parse(sdl, options={strict: true}){
        let normalSdl = this.normailzeSDL(sdl);
        for(let i=0; i < normalSdl.length; i++){
            this.processToken(normalSdl[i], i, normalSdl);
        }
        return this.dom;
    }
}

export {SDLParser};