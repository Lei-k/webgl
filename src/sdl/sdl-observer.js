class SDLMutationObserver {
    constructor(callback){
        this.callback = callback;
    }
}

export {SDLMutationObserver};