import {SceneManager} from '../scene/scene.js'
import {Shader, ShaderProgram} from '../resources/shader.js'
import {createCubeMesh} from './geometry.js'
import {GLContextManager} from './contex-manager.js'
import {Material} from '../resources/material.js';
import {Resources} from '../resources/resources.js'
import {RealEvent, Real} from './core.js';

function drawScene(deltaTime){
    const gl = GLContextManager.gl;
    gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    gl.clearDepth(1.0);                 // Clear everything
    gl.enable(gl.DEPTH_TEST);           // Enable depth testing
    gl.depthFunc(gl.LEQUAL);            // Near things obscure far things
    gl.enable(gl.CULL_FACE);
    gl.cullFace(gl.BACK);

    // Clear the canvas before we start drawing on it.
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    let scenes = SceneManager.scenes;
    scenes.forEach((s, k) => {
        if(s.world){
            s.world.calcWorldTransform();
            s.world.traverseUpdateAndRender(deltaTime);
        }
    })
}

function loadShader(type, source) {
    const shader = new Shader(source, type);
    shader.upload();
    shader.compile();
    return shader;
}

function initShaderProgram(){
    const ps = [];
    ps.push(fetch('../../resources/vertex_shader/phong.vs'));
    ps.push(fetch('../../resources/fragment_shader/phong.fs'));
    ps.push(fetch('../../resources/vertex_shader/skybox.vs'));
    ps.push(fetch('../../resources/fragment_shader/skybox.fs'));
    const p = Promise.all(ps).then(response =>{
        const ps = response.map(r => r.text());
        return Promise.all(ps);
    }).then(texts =>{
        const gl = GLContextManager.gl;
        const programs = [];
        for(let i=0; i<texts.length; i+=2){
            const program = new ShaderProgram();
            const vs = loadShader(gl.VERTEX_SHADER, texts[i]);
            const fs = loadShader(gl.FRAGMENT_SHADER, texts[i+1]);
            program.attach(vs);
            program.attach(fs);
            program.link();
            programs.push(program);
        }
        return new Promise((resolve) => {resolve(programs);})
    })

    return p;
}

function glMain(){
    const gl = GLContextManager.findContext('webgl-canvas');

    Resources.registerMesh('cube', createCubeMesh())

    const promise = initShaderProgram();

    promise.then(function(programs){
        const shaderProgram = programs[0];
        shaderProgram.use();
        const mesh = Resources.findMesh('cube');

        let material = new Material(shaderProgram);
        mesh.bindMaterial(material);
        Resources.registerMaterial('default', material);
        material = new Material(programs[1]);
        Resources.registerMaterial('skybox', material);
    
        shaderProgram.uniform1i('uSampler', 0);

        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
        Real.dispatchEvent(new RealEvent('load'));
        var then = 0;
        // Draw the scene repeatedly
        function render(now) {
            now *= 0.001;  // convert to seconds
            const deltaTime = now - then;
            then = now;
    
            drawScene(deltaTime);
    
            requestAnimationFrame(render);
        }
        requestAnimationFrame(render);
    })
}

var backend = {glMain};

export {backend};