import {GLContextManager} from './contex-manager.js';

class RealEvent {
    constructor(name){
        this.name = name;
    }
}

class RealEventTarget {
    constructor(){
        this.eventCallbacks = new Map();
    }

    addEventListener(name='', callback=(event=RealEvent)=>{}){
        let callbacks = this.eventCallbacks.get(name);
        if(!callbacks){
            callbacks = [];
            this.eventCallbacks.set(name, callbacks);
        }

        callbacks.push(callback);
    }

    dispatchEvent(event){
        const callbacks = this.eventCallbacks.get(event.name);
        if(!callbacks) return;
        callbacks.forEach(callback => callback(event));
    }
}

const realEventTarget = new RealEventTarget();
class Real {
    static addEventListener(name='', callback=(event)=>{}){
        realEventTarget.addEventListener(name, callback);
    }

    static dispatchEvent(event){
        realEventTarget.dispatchEvent(event);
    }
}

class GLComponent extends RealEventTarget {
    constructor(){
        super();
        this.gl = GLContextManager.gl;
    }

    bindGLContext(gl){
        this.gl = gl;
    }
}

class RealNode extends RealEventTarget {
    constructor(){
        super()
        this.parent = undefined;
        this.children = [];
        this.sibling = undefined;
    }

    addChild(child=RealNode){
        if(this.children.length > 0){
            let idx = this.children.length-1;
            this.children[idx].sibling = child;
        }
        this.children.push(child);
        child.parent = this;
    }

    removeChild(child=RealNode, recursive=true){
        this.children = this.children.filter(c => c !== child);

        if(recursive){
            this.children.forEach(function(c){
                c.removeChild(child, recursive);
            })
        }
    }

    traverse(callback=(node=RealNode) => {}){
        callback(this);

        let children = this.children;
        children.forEach(function(child){
            child.traverse(callback);
        })
    }

    traverseUntil(callback=(node=RealNode) => {return false;}){
        if(callback(this)) return;

        let children = this.children;
        children.forEach(function(child){
            child.traverseUntil(callback);
        })
    }
}

export {RealNode, GLComponent, RealEventTarget, RealEvent, Real};