import {Material} from '../resources/material.js';
import { GLComponent } from './core.js';
import { Vector3, Vector4, Vector2 } from '../math/math.js';
import {Vbo} from '../resources/object.js'
import { SceneManager } from '../scene/scene.js';
import { GLContextManager } from './contex-manager.js';
import {Vao} from '../resources/object.js'

function cube(){
    const positions = [
        // Front face
        -1.0, -1.0,  1.0,
         1.0, -1.0,  1.0,
         1.0,  1.0,  1.0,
        -1.0,  1.0,  1.0,
        
        // Back face
        -1.0, -1.0, -1.0,
        -1.0,  1.0, -1.0,
         1.0,  1.0, -1.0,
         1.0, -1.0, -1.0,
        
        // Top face
        -1.0,  1.0, -1.0,
        -1.0,  1.0,  1.0,
         1.0,  1.0,  1.0,
         1.0,  1.0, -1.0,
        
        // Bottom face
        -1.0, -1.0, -1.0,
         1.0, -1.0, -1.0,
         1.0, -1.0,  1.0,
        -1.0, -1.0,  1.0,
        
        // Right face
         1.0, -1.0, -1.0,
         1.0,  1.0, -1.0,
         1.0,  1.0,  1.0,
         1.0, -1.0,  1.0,
        
        // Left face
        -1.0, -1.0, -1.0,
        -1.0, -1.0,  1.0,
        -1.0,  1.0,  1.0,
        -1.0,  1.0, -1.0,
    ];

    const faceColors = [
        [1.0,  1.0,  1.0,  1.0],    // Front face: white
        [1.0,  0.0,  0.0,  1.0],    // Back face: red
        [0.0,  1.0,  0.0,  1.0],    // Top face: green
        [0.0,  0.0,  1.0,  1.0],    // Bottom face: blue
        [1.0,  1.0,  0.0,  1.0],    // Right face: yellow
        [1.0,  0.0,  1.0,  1.0],    // Left face: purple
    ];

    const indices = [
        0,  1,  2,      0,  2,  3,    // front
        4,  5,  6,      4,  6,  7,    // back
        8,  9,  10,     8,  10, 11,   // top
        12, 13, 14,     12, 14, 15,   // bottom
        16, 17, 18,     16, 18, 19,   // right
        20, 21, 22,     20, 22, 23,   // left
    ];

    const textureCoordinates = [
        // Front
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Back
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Top
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Bottom
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Right
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
        // Left
        0.0,  0.0,
        1.0,  0.0,
        1.0,  1.0,
        0.0,  1.0,
      ];

    const vertexNormals = [
        // Front
        0.0,  0.0,  1.0,
        0.0,  0.0,  1.0,
        0.0,  0.0,  1.0,
        0.0,  0.0,  1.0,

        // Back
        0.0,  0.0, -1.0,
        0.0,  0.0, -1.0,
        0.0,  0.0, -1.0,
        0.0,  0.0, -1.0,

        // Top
        0.0,  1.0,  0.0,
        0.0,  1.0,  0.0,
        0.0,  1.0,  0.0,
        0.0,  1.0,  0.0,

        // Bottom
        0.0, -1.0,  0.0,
        0.0, -1.0,  0.0,
        0.0, -1.0,  0.0,
        0.0, -1.0,  0.0,

        // Right
        1.0,  0.0,  0.0,
        1.0,  0.0,  0.0,
        1.0,  0.0,  0.0,
        1.0,  0.0,  0.0,

        // Left
        -1.0,  0.0,  0.0,
        -1.0,  0.0,  0.0,
        -1.0,  0.0,  0.0,
        -1.0,  0.0,  0.0
    ];

    return {positions: positions, 
            faceColors: faceColors, 
            indices: indices, 
            textureCoord: textureCoordinates,
            normals: vertexNormals}
}

var MaterialMappingMode = {
    ALL_SAME:0, BY_POLYGON:1, BY_TRIANGLE:2
}

var DrawMode = {
    ARRAY: 0, ELEMENT: 1
}

class Vertex {
    constructor(position, color, normal, uvs){
        this.position = position;
        this.color = color;
        this.normal = normal;
        this.uvs = uvs;
    }
}

class VertexBuffers {
    constructor(){
        
    }

    static makeFromVao(vao){
        return this.makeFromVbos(vao.attributes);
    }

    static makeFromVbos(attributes={position:Vbo, color:Vbo, normal:Vbo, uvs:[Vbo]}){
        const arrayBuffers = {};
        arrayBuffers.position = attributes.position.data;
        arrayBuffers.color = attributes.color.data;
        arrayBuffers.normal = attributes.normal.data;
        arrayBuffers.uvs = attributes.uvs.map(uv => uv.data);
        return this.makeFromArrayBuffers(arrayBuffers);
    }

    static makeFromArrayBuffers(arrayBuffers={position:[], color:[], normal:[], uvs:[]}){
        const position = arrayBuffers.position;
        const color = arrayBuffers.color;
        const normal = arrayBuffers.normal;
        const uvs = arrayBuffers.uvs;
        let pos = undefined;
        if(position && position.length > 0){
            pos = [];
            for(let i=0; i < position.length; i+=3){
                pos.push(new Vector3(position[i], position[i+1], position[i+2]));
            }
        }
        
        let cs = undefined;
        if(color && color.length > 0){
            cs = [];
            for(let i=0; i < color.length; i+=4){
                cs.push(new Vector4(color[i], color[i+1], color[i+2], color[i+3]));
            }
        }
        
        let n = undefined;
        if(normal && normal.length > 0){
            n = [];
            for(let i=0; i < color.length; i+=4){
                n.push(new Vector3(normal[i], normal[i+1], normal[i+2]));
            }
        }

        let nuvs = undefined;
        if(uvs && uvs.length>0){
            nuvs = [];
            for(let i=0; i < uvs[0].length; i++){
                const tuvs = [];
                for(let j=0; j < uvs.length; j++){
                    tuvs.push(new Vector2(uvs[j][i], uvs[j][i+1]));
                }
                nuvs.push(tuvs);
            }
        }

        let vertices = [];
        pos.forEach(function(p, idx){
            vertices.push(new Vertex(p, cs[idx], n[idx], nuvs[idx]));
        })
        return vertices;
    }

    static makeFromVertices(vertices){
        const position = vertices.map(v => v.position);
        const color = vertices.map(v => v.color);
        const normal = vertices.map(v => v.normal);
        const uvs = vertices[0].uvs;
        uvs.forEach(function(uv, idx){
            uvs[idx] = vertices.map(v => v.uvs[idx]);
        })
        return {position: position, color: color, normal: normal, uvs: uvs};
    }
}

class VertexArrays {
    constructor(positions, colors, indices, textureCoord, normals){
        this.positions = positions;
        this.colors = colors;
        this.indices = indices;
        this.textureCoord = textureCoord;
        this.normals = normals;
    }

    makeVao(){
        const gl = GLContextManager.gl;

        let posVbo = this.positions ? new Vbo(new Float32Array(this.positions), gl.ARRAY_BUFFER) : undefined;
        let colorVbo = this.colors ? new Vbo(new Float32Array(this.colors), gl.ARRAY_BUFFER) : undefined;
        let indicesVbo = this.indices ? new Vbo(new Uint16Array(this.indices), gl.ELEMENT_ARRAY_BUFFER) : undefined;
        let textureCoordVbo = this.textureCoord ? new Vbo(new Float32Array(this.textureCoord), gl.ARRAY_BUFFER) : undefined;
        let normalVbo = this.normals ? new Vbo(new Float32Array(this.normals), gl.ARRAY_BUFFER) : undefined;

        const vao = new Vao();
        if(posVbo) vao.addVbo('position', posVbo);
        if(colorVbo) vao.addVbo('color', colorVbo);
        if(indicesVbo) vao.addVbo('indices', indicesVbo);
        if(textureCoordVbo) vao.addVbo('uv', textureCoordVbo);
        if(textureCoordVbo) vao.addVbo('normal', normalVbo);
        if(normalVbo) vao.upload(gl.STATIC_DRAW);

        return vao;
    }
}

class Triangle {
    constructor(vertexIndices, startIndices, material){
        this.vertexIndices = vertexIndices;
        this.startIndices = startIndices;
        this.material = material;
    }
}

class Polygon {
    constructor(startIndices, size, material){
        this.startIndices = startIndices;
        this.size = size;
        this.material = material;
    }

    // assume triangles is sorted by mtls
    static buildPolygonsFromTriangles(triangles){
        let ps = [];
        let p = new Polygon();
        triangles.forEach(function(t, idx, ts){
            if(idx === 1){
                p.startIndices = t.startIndices;
                return;
            }
            if(t.material !== ts[idx-1]){
                p.size = ts[idx-1].startIndices - p.startIndices + 3;
                p.material = ts[idx-1].material;
                ps.push(p);
                p = new Polygon();
                p.startIndices = t.startIndices;
            }
        })
        return ps;
    }
}

class Mesh extends GLComponent {
    constructor(vao, indeices){
        super();
        this.vao = vao;
        //this.vertices = VertexBuffers.makeFromVao(vao);
        this.materialMappingMode = MaterialMappingMode.ALL_SAME;
        this.startIndices = 0;
        this.material = undefined;
        this.indeices = indeices;
        this.drawMode = DrawMode.ARRAY;
        this.size = indeices.length;
    }

    static makeFromVertexArrays(vertexArrays){
        const vao = vertexArrays.makeVao();
        
        const mesh = new Mesh(vao, vertexArrays.indices);
        mesh.setDrawMode(DrawMode.ELEMENT);
        return mesh;
    }

    bindMaterial(material){
        this.material = material;
        const program = material.program;
        const size = program.getParameter(this.gl.ACTIVE_ATTRIBUTES);
        const names = [];
        for(let i=0; i < size; i++){
            names.push(program.getActiveAttrib(i).name);
        }
        for (const [key, attribute] of Object.entries(this.vao.attributes)) {
            if(key === 'uvs'){
                let that = this;
                attribute.forEach(function(vbo, idx){
                    if(!names.includes('uv'+idx)) return;
                    vbo.bind();
                    program.vertexAttribPointer('uv'+idx, 2, that.gl.FLOAT);
                    program.enableVertexAttrib('uv'+idx);
                })
            }else{
                if(!names.includes(key)) continue;
                attribute.bind();
                program.vertexAttribPointer(key, 3, this.gl.FLOAT);
                program.enableVertexAttrib(key);
            }
        }
    }

    setDrawMode(mode){
        this.drawMode = mode;
    }

    renderByDrawMode(offset, verticesCount){
        this.vao.bind();
        const gl = this.gl;
        const program = this.material.program;
        const camera = SceneManager.camera;
        if(program && camera){
            program.uniformMatrix4fv('projectionMatrix', camera.projectionMatrix.elements);
            program.uniformMatrix4fv('viewMatrix', camera.viewMatrix.elements);
        }
        
        if(this.drawMode === DrawMode.ARRAY){
            gl.drawArrays(gl.TRIANGLE_STRIP, offset, verticesCount);
        }else if(this.drawMode === DrawMode.ELEMENT){
            const type = gl.UNSIGNED_SHORT;
            gl.drawElements(gl.TRIANGLES, verticesCount, type, offset);
        }else {
            console.warn('draw mode not definded');
        }
    }

    render(){
        const mode = this.materialMappingMode;
        if(mode === MaterialMappingMode.ALL_SAME){
            if(!this.material){
                console.warn('material not exists');
                return;
            }
            this.material.use();
            this.renderByDrawMode(this.startIndices, this.size);
        }else if(mode === MaterialMappingMode.BY_POLYGON){
            if(!this.polygons){
                console.warn('does not has polygons');
                return;
            }
            this.polygons.forEach(function(p, idx){
                if(!p.material){
                    console.warn('material not exists');
                    return;
                }
                p.material.use();
                this.renderByDrawMode(p.startIndices, size);
            })
        }else if(mode == MaterialMappingMode.BY_TRIANGLE){
            if(!this.triangles){
                console.warn('does not has triangles');
                return;
            }
            this.triangles.forEach(function(t, idx){
                if(!t.material){
                    console.warn('material not exists');
                    return;
                }
                t.material.use();
                this.renderByDrawMode(t.startIndices, size);
            })
        }else {
            console.warn('material mapping mode not definded');
        }
    }
}

var Geometry = {
    cube: cube
}

function createCubeMesh(){
    const cubeShape = Geometry.cube();

    const positions = cubeShape.positions;
    const faceColors = cubeShape.faceColors;
    const indices = cubeShape.indices;
    const textureCoord = cubeShape.textureCoord;
    const normals = cubeShape.normals;

    let colors = [];
    for (var j = 0; j < faceColors.length; ++j) {
        const c = faceColors[j];
    
        // Repeat each color four times for the four vertices of the face
        colors = colors.concat(c, c, c, c);
    }

    let arrays = new VertexArrays(positions, faceColors, indices, textureCoord, normals);
    const mesh = Mesh.makeFromVertexArrays(arrays);
    return mesh;
}

export {Geometry, Vertex, Triangle, Polygon, Mesh, VertexBuffers, DrawMode, VertexArrays, createCubeMesh}