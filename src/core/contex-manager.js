var sgl = undefined;
var scanvas = undefined;
class GLContextManager {
    static bind(gl){
        sgl = gl;
    }

    static get gl(){
        return sgl;
    }

    static get canvas(){
        return scanvas;
    }

    static findContext(id=''){
        scanvas = document.querySelector("#"+id);
        // 初始化 GL context
        const gl = scanvas.getContext("webgl");
    
        // 當 WebGL 有效才繼續執行
        if (gl === null) {
            alert("無法初始化 WebGL，您的瀏覽器不支援。");
            return;
        }

        sgl = gl;
        return sgl;
    }
}

export {GLContextManager};