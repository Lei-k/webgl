var keys = new Array(256);
keys.forEach(function(key, idx, array){
    array[idx] = false;
})

class Input {
    static key(key){
        const type = typeof key;
        if(type === 'number'){
            return keys[key];
        }else if(type === 'string'){
            return keys[key.charCodeAt(0)];
        }else{
            console.warn('this key type ' + typeof key + ' not definded');
        }
    }

    static setKey(keyCode, value){
        keys[keyCode] = value;
    }
}

export {Input};