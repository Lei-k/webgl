import {RealNode} from '../core/core.js';
import {Vector3, Euler, Matrix4, Transform, TransformMatrices} from '../math/math.js';
import {GLContextManager} from '../core/contex-manager.js';
import { Texture } from '../resources/texture.js';
import {Resources} from '../resources/resources.js';
import {SDLParser} from '../sdl/sdl-parser.js';
import {SDLCameraElement} from '../sdl/sdl-elements.js'
import { SceneBuilder } from './scene-builder.js';
import {RealEvent, RealEventTarget} from '../core/core.js';

class Scene extends RealEventTarget {
    constructor(gl){
        super();
        this.gl = gl;
    }

    resize(w, h){
        this.gl.canvas.width = w;
        this.gl.canvas.height = h;
        this.gl.viewport(0, 0, w, h);
    }

    load(url){
        let that = this;
        return new Promise((resolve, reject) => {
            GLContextManager.bind(this.gl)
            fetch(url).then(response => {
                return response.text();
            }).then(text => {
                if(!that.parser){
                    that.parser = new SDLParser();
                }
    
                that.document = that.parser.parse(text);
    
                if(!that.sceneBuilder){
                    that.sceneBuilder = new SceneBuilder();
                }
    
                that.world = that.sceneBuilder.build(that.document);
                resolve(that)
            }).catch(reason => {
                reject(reason);
            })
        });
    }
}

var sCamera;
var sscenes = new Map();
class SceneManager {
    static bindCamera(camera){
        if(camera instanceof SDLCameraElement){
            sCamera = camera.sceneNode;
        }else if(camera instanceof Camera){
            sCamera = camera;
        }else{
            console.warn('binding instance is not a accept camera type')
        }
    }

    static get camera(){
        return sCamera;
    }

    static loadScene(url, gl, options={sceneName}){
        let s = new Scene(gl);
        const promise = s.load(url);
        let sceneName = options.sceneName;
        if(!sceneName) sceneName = url;
        sscenes.set(sceneName, s);
        return promise;
    }

    static scene(sceneName){
        return sscenes.get(sceneName);
    }

    static get scenes(){
        return sscenes;
    }
}

class SceneNode extends RealNode {
    constructor(){
        super();
        this.transform = new Transform();
        this.worldTransform = new Transform();
        this.visiable = true;
        this.modelMatrix = new Matrix4();
        this.transformMatrices = new TransformMatrices();
        this.worldTransformMatrices = new TransformMatrices();
        this.changes = {position: false, scale: false, rotation: false}
    }

    traverseRender(){
        this.traverse(node => {
            if(!node.visiable) return;
            node.render();
        })
    }

    traverseUpdate(deltaTime){
        this.traverse(node => {
            node.update(deltaTime);
        })
    }

    traverseUpdateAndRender(deltaTime){
        this.traverseUpdate(deltaTime);
        this.traverseRender();
    }

    extractChangeRoots(){
        const changeRoots = [];
        
        this.traverseUntil(node => {
            const f = node.changes.position || node.changes.scale || node.changes.rotation;
            if(f) changeRoots.push(node);
            return f;
        })
        return changeRoots;
    }

    calcWorldTransform(){
        // do the changes propagation
        this.traverse(node => {
            const children = node.children;
            children.forEach((c) => {
                c.changes.position = node.changes.position || node.changes.rotation? true : c.changes.position;
                c.changes.scale = node.changes.scale? true : c.changes.scale;
                c.changes.rotation = node.changes.rotation? true : c.changes.rotation;
            })
        })

        const changeRoots = this.extractChangeRoots();

        changeRoots.forEach(function(root){
            root.traverse(node => {
                const change = node.changes.position || node.changes.rotation || node.changes.scale;

                let parent = node.parent;
                if(parent){
                    const pwtmices = parent.worldTransformMatrices;
                    if(node.changes.rotation){
                        node.transformMatrices.rotation = Matrix4.makeFromEuler(node.transform.rotation);
                        node.worldTransformMatrices.rotation = Matrix4.multiply(pwtmices.rotation, node.transformMatrices.rotation);
                        node.worldTransform.rotation = Euler.makeFromMatrix(node.worldTransformMatrices.rotation);
                    }

                    if(node.changes.position){
                        node.worldTransform.position = 
                        Vector3.add(parent.worldTransform.position, Vector3.transform(node.transform.position, pwtmices.rotation));
                    }

                    if(node.changes.scale){
                        node.worldTransform.scale = Vector3.multiply(parent.worldTransform.scale, node.transform.scale);
                    }
                }else{
                    if(change){
                        node.worldTransform = node.transform.copy();
                    }
                    
                    if(node.changes.rotation){
                        node.worldTransformMatrices.rotation = Matrix4.makeFromEuler(node.transform.rotation);
                    }
                }

                if(node.changes.position){
                    node.worldTransformMatrices.translate = Matrix4.makeFromTranslation(node.worldTransform.position);
                }
                
                if(node.changes.scale){
                    node.worldTransformMatrices.scale = Matrix4.makeFromScaling(node.worldTransform.scale);
                }

                if(change){
                    node.modelMatrix = Matrix4.multiply(node.worldTransformMatrices.translate, node.worldTransformMatrices.rotation);
                    node.modelMatrix = Matrix4.multiply(node.modelMatrix, node.worldTransformMatrices.scale);
                }
                
                node.changes.position = node.changes.rotation = node.changes.scale = false;
            })
        })
    }

    render(){}

    update(deltaTime){
        if(this.sdlElement){
            const event = new RealEvent('update');
            event.target = this.sdlElement;
            event.deltaTime = deltaTime;
            this.sdlElement.dispatchEvent(event);
            if(this.sdlElement.onupdate){
                this.sdlElement.onupdate(event);
            }
        }
    }
}

class SceneObject extends SceneNode {
    constructor(){
        super();
        this.mesh = undefined;
    }

    bindMesh(mesh){
        this.mesh = mesh;
    }

    render(){
        const mesh = this.mesh;
        let program;
        if(mesh){
            const material = mesh.material;
            if(material){
                program = material.program;
            }
        }
        if(this.texture){
            this.texture.bind();
        }
        if(program){
            program.use();
            program.uniformMatrix4fv('modelMatrix', this.modelMatrix.elements);
            program.uniform1i('uSampler', 0);
            const camera = SceneManager.camera;
            let viewMatrix;
            if(camera){
                viewMatrix = camera.viewMatrix;
            }else{
                viewMatrix = new Matrix4();
            }
            const modelViewMatrix = Matrix4.multiply(viewMatrix, this.modelMatrix);
            let normalMatrix = Matrix4.invert(modelViewMatrix);
            normalMatrix = Matrix4.transpose(normalMatrix);

            program.uniformMatrix4fv('normalMatrix', normalMatrix.elements);
        }
        if(this.mesh){
            this.mesh.render();
        }
    }
}

class Skybox extends SceneNode {
    constructor(){
        super();
        this.textures = new Array(6);
    }

    load(front, back, right, left, up, down){
        this.mesh = Resources.findMesh('cube');
        for(let i=0; i < arguments.length; i++){
            if(arguments[i]){
                const url = arguments[i];
                let texture = Resources.findTexture(url);
                if(!texture){
                    texture = new Texture();
                    texture.load(url);
                    texture.bind();
                    Resources.registerTexture(url, texture);
                }
                this.textures[i] = texture;
            }
        }
        this.front = front;
        this.back = back;
        this.right = right;
        this.left = left;
        this.down = down;
        this.up = up;
    }

    render(){
        if(!this.mesh){
            this.mesh = Resources.findMesh('cube');
        }

        const material = Resources.findMaterial('skybox');
        if(material){
            material.use();
            if(this.mesh){
                this.mesh.vao.bind();
            }
            const program = material.program;
            program.uniformMatrix4fv('modelMatrix', this.modelMatrix.elements);
            program.uniform1i('uSampler', 0);
            const camera = SceneManager.camera;
            let viewMatrix;
            if(camera){
                viewMatrix = camera.viewMatrix;
            }else{
                viewMatrix = new Matrix4();
            }
            const modelViewMatrix = Matrix4.multiply(viewMatrix, this.modelMatrix);
            let normalMatrix = Matrix4.invert(modelViewMatrix);
            normalMatrix = Matrix4.transpose(normalMatrix);

            program.uniformMatrix4fv('normalMatrix', normalMatrix.elements);

            if(camera){
                program.uniformMatrix4fv('projectionMatrix', camera.projectionMatrix.elements);
                program.uniformMatrix4fv('viewMatrix', camera.viewMatrix.elements);
            }
        }
        
        const gl = GLContextManager.gl;
        if(!gl){
            return;
        }
        const type = gl.UNSIGNED_SHORT;
        gl.cullFace(gl.FRONT);
        for(let i=0; i<6;i++){
            let idx = i;
            if(this.textures[i]){
                if(idx === 2){
                    idx = 4;
                }else if(idx === 3){
                    idx = 5;
                }else if(idx === 4){
                    idx = 2;
                }else if(idx === 5){
                    idx = 3;
                }
                this.textures[idx].bind();
                gl.drawElements(gl.TRIANGLES, 6, type, i*6*2);
            }
        }
        gl.cullFace(gl.BACK);
    }
}

class Camera extends SceneNode {
    constructor(){
        super();
        this.up = new Vector3(0, 1, 0);
        this.direction = new Vector3(0, 0, -1);
        this.viewMatrix = new Matrix4();
        this.projectionMatrix = new Matrix4();
    }

    update(deltaTime){
        super.update(deltaTime);
        const rotationMatrix = Matrix4.makeFromEuler(this.worldTransform.rotation);
        const direction = Vector3.transform(this.direction, rotationMatrix);
        const lookat = Vector3.add(this.worldTransform.position, direction);
        const up = Vector3.transform(this.up, rotationMatrix);
        this.viewMatrix = Matrix4.lookat(this.worldTransform.position, lookat, up);
    }
}

class PerspectiveCamera extends Camera {
    constructor(){
        super();
        this.fieldOfView = 45 * Math.PI / 180;   // in radians
        this.near = 0.1;
        this.far = 1000.0;
    }

    update(deltaTime){
        super.update(deltaTime);
        const gl = GLContextManager.gl;
        const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
        this.projectionMatrix = Matrix4.perspective(this.fieldOfView, aspect, this.near, this.far);
    }
}

class DirectionalLight extends SceneNode {
    constructor(){
        super();
        this.direction = new Vector3();
    }
}

class PointLight extends SceneNode {
    constructor(){
        super();
    }
}

class SpotLight extends SceneNode {
    constructor(){
        super();
        this.direction = new Vector3();
    }
}

export {SceneManager, SceneNode, SceneObject, Skybox, Camera, DirectionalLight, PointLight, SpotLight, PerspectiveCamera}