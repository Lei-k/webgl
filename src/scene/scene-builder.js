import {SceneFactory} from '../scene/scene-factory.js'

class SceneBuilder {
    constructor(){
        this.worldTree;
    }

    parseElement(element){
        let factory = SceneFactory.factory(element.tagName)
        let result = factory.makeFromElement(element);
        if(element && result){
            result.sdlElement = element;
            if(element.tagName === 'WORLD'){
                this.worldTree = result;
            }

            if(!element.position){
                element.position = result.transform.position;
            }
            if(!element.rotation){
                element.rotation = result.transform.rotation;
            }
            if(!element.scale){
                element.scale = result.transform.scale;
            }
        }
        return result;
    }

    build(dom){
        let that = this;
        dom.traverse(element => {
            let rs = that.parseElement(element);
            if(!rs) return;
            element.sceneNode = rs;

            let parent = element.parent;
            if(parent && parent.sceneNode){
                parent.sceneNode.addChild(rs);
            }
        })
        return this.worldTree;
    }
}

export {SceneBuilder};