import {SceneNode, SceneObject, Skybox, Camera, DirectionalLight, PointLight, SpotLight, PerspectiveCamera} from './scene.js';
import { Resources } from '../resources/resources.js';
import { Texture } from '../resources/texture.js';

class SceneNodeFactory {
    constructor(){
        
    }

    supportAttrs(){
        return ['position', 'scale', 'rotation', 'onupdate'];
    }

    supportAttr(attrName){
        return this.supportAttrs().includes(attrName);
    }

    parseElementAttr(name, value){
        return {name: name, value: value};
    }

    parseElementAttrsToSceneNode(element, sceneNode){
        let attrs = element.attributes;
        let that = this;
        attrs.forEach(function(value, key){
            if(!that.supportAttr(key)) return;

            let attr = that.parseElementAttr(key, value);

            let isTransformProperty = false;
            for(let skey in sceneNode.transform){
                if(skey === attr.name){
                    isTransformProperty = true;
                    break;
                }
            }
            if(isTransformProperty){
                sceneNode.transform[attr.name] = attr.value;
                sceneNode.changes[attr.name] = true;
            }else{
                sceneNode[attr.name] = attr.value;
            }
        })
        return sceneNode;
    }

    makeFromElement(element){
        let result = undefined;
        let tagName = element.tagName;
        if(tagName === 'MODEL'){
            result = new SceneObject();
        }else if(tagName === 'CAMERA'){
            result = new Camera();
            switch(element.getAttribute('type')){
                case 'perspective': default:
                    result = new PerspectiveCamera();
                    break;
            }
        }else if(tagName === 'LIGHT'){
            switch(element.attributes.get('type')){
                case 'directional': default:
                    result = new DirectionalLight();
                    break;
                case 'point':
                    result = new PointLight();
                    break;
                case 'spot':
                    result = new SpotLight();
                    break;
            }
        }else if(tagName === 'WORLD'){
            // the world tag identify root node
            // of scene graph. it hasn't any entity
            // to render or do something so just initialize it
            // to a scene node instance
            result = new SceneNode();
        }else if(tagName === 'SKYBOX'){
            result = new Skybox();
        }else {
            result = new SceneNode();
        }
    
        if(result){
            result = this.parseElementAttrsToSceneNode(element, result);
        }
        return result;
    }
}

class SceneObjectFactory extends SceneNodeFactory {
    constructor(){
        super();
    }

    supportAttrs(){
        let supports = super.supportAttrs();
        supports.push('mesh');
        supports.push('texture');
        return supports;
    }

    parseElementAttr(name, value){
        if(name === 'mesh'){
            let mesh = Resources.findMesh(value);
            return {name: name, value: mesh};
        }else if(name === 'texture'){
            const url = value;
            let texture = Resources.findTexture(url);
            if(!texture){
                texture = new Texture();
                texture.load(url);
                texture.bind();
                Resources.registerTexture(url, texture);
            }
            return {name: name, value: texture};
        }
        return super.parseElementAttr(name, value);
    }
}

class CameraFactory extends SceneNodeFactory {
    constructor(){
        super();
    }
}

class SkyboxFactory extends SceneNodeFactory {
    constructor(){
        super();
    }

    makeFromElement(element){
        let skybox = new Skybox();
        const front = element.front;
        const back = element.back;
        const right = element.right;
        const left = element.left;
        const up = element.up;
        const down = element.down;
        skybox.load(front, back, right, left, up, down);
        skybox = this.parseElementAttrsToSceneNode(element, skybox);
        return skybox;
    }
}

const nodefactory = new SceneNodeFactory();
const objectfactory = new SceneObjectFactory();
const camerafactory = new CameraFactory();
const skyboxfactory = new SkyboxFactory();

class SceneFactory {
    static factory(type){
        switch(type){
            case 'MODEL':
            return objectfactory;
            case 'CAMERA':
            return camerafactory;
            case 'SKYBOX':
            return skyboxfactory;
            default: return nodefactory;
        }
    }
}

export {SceneFactory}