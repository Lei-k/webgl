import {Real} from '../src/real.js';
import {Euler} from '../src/real.js';
import {Input} from '../src/real.js'; 
import {Matrix4, Vector3} from '../src/math/math.js';
import {GLContextManager} from '../src/core/contex-manager.js'
import {SceneManager} from '../src/scene/scene.js';

const textures = ['./resources/fds.png', './resources/stitch1.jpg', './resources/Lisa.jpg'];

var squareRotation = 0.0;
var flag = 1;
var alpha = 0.1;
var lowpassDeltaTime = 1;
var lastlowpassDetlaTime = lowpassDeltaTime;
var camera;

var ctime = new Date();
var canvasScene;

function cameraRotationUpdate(event){

    const deltaX = event.movementX;
    const deltaY = event.movementY;

    const euler = new Euler(-deltaY*0.005, -deltaX*0.005, 0);
    const rotation = Matrix4.makeFromEuler(euler);
    const cameraRotation = camera.rotationMatrix;
    const r = Matrix4.multiply(cameraRotation, rotation);
    camera.rotation = Euler.makeFromMatrix(r);
}

function setUpScene(scene){
    const sdldom = scene.document;
    console.log(sdldom)
    console.log(scene.world)
    var ntime = new Date();
    console.log('realjs load time: ' + (ntime.getTime() - ctime.getTime()) + 'ms')

    var boxGroup = sdldom.querySelector('#box-group');
    sdldom.querySelector('#world').onupdate = function(event){
        const deltaTime = event.deltaTime;
        squareRotation += deltaTime;
        lowpassDeltaTime += alpha * (deltaTime - lastlowpassDetlaTime);
        lastlowpassDetlaTime = lowpassDeltaTime;
        document.querySelector('#fps').innerHTML = 'fps: ' + ~~(1 / lowpassDeltaTime + 0.5);
        document.querySelector('#rotation').innerHTML = 'rotation: ' + ~~(squareRotation * 180 / Math.PI) % 360;
    }
    
    boxGroup.addEventListener('update', event => {
        const deltaTime = event.deltaTime;
        event.target.position.x += flag * 2 *deltaTime;
        event.target.position.y += flag * 2 *deltaTime;
        if(event.target.position.x > 4){
            flag = -1;
        }
        if(event.target.position.x < -4){
            flag = 1;
        }
        event.target.rotation = new Euler(squareRotation, 0, squareRotation);
    
        let r = squareRotation * 50 % 360;
        const children = event.target.children;
        children.forEach(function(child){
            child.rotation = new Euler(squareRotation, 0, squareRotation);
            if(r > 240){
                child.texture = textures[2];
            }else if( r > 120) {
                child.texture = textures[1];
            }else {
                child.texture = textures[0];
            }
        })
    })

    setInterval(() => {
        const x = Math.random() * 100 - 50, y = Math.random() * 100 - 50, z = Math.random() * 100 - 50;
        boxGroup.append('<model mesh="cube" position="(' + x + ', ' + y + ', ' + z + ')" texture="./resources/fds.png"></model>');
    }, 200)
    
    camera = sdldom.querySelector("#camera");
    SceneManager.bindCamera(camera)
    camera.onupdate = function(event){
        const target = event.target;
        const deltaTime = event.deltaTime;
        const speed = 10;
        const d = speed * deltaTime;
        const rotationMatrix = target.rotationMatrix;
        if(Input.key('w')){
            let v = new Vector3(0, 0, d);
            v = Vector3.transform(v, rotationMatrix);
            target.position = Vector3.subtract(target.position, v);
        }
        if(Input.key('s')){
            let v = new Vector3(0, 0, d);
            v = Vector3.transform(v, rotationMatrix);
            target.position = Vector3.add(target.position, v);
        }
        if(Input.key('d')){
            let v = new Vector3(d, 0, 0);
            v = Vector3.transform(v, rotationMatrix);
            target.position = Vector3.add(target.position, v);
        }
        if(Input.key('a')){
            let v = new Vector3(d, 0, 0);
            v = Vector3.transform(v, rotationMatrix);
            target.position = Vector3.subtract(target.position, v);
        }
    }

    let ui = document.querySelector('#ui-main');
    let canvas = GLContextManager.canvas;
    ui.onclick = function(){
        ui.style.display = 'none';
        canvas.requestPointerLock();
    }

    canvas.onclick = function(){
        document.exitPointerLock();
        ui.style.display = 'block';
    }

    document.addEventListener('pointerlockchange', function(){
        if(document.pointerLockElement === canvas){
            window.addEventListener('mousemove', cameraRotationUpdate)
        }else{
            window.removeEventListener('mousemove', cameraRotationUpdate);
        }
    })
}

// after realjs framwork is loaded
Real.addEventListener('load', (event) => {
    const gl = GLContextManager.findContext('webgl-canvas');
    SceneManager.loadScene('./sdl.xml', gl, {sceneName: 'scene'})
    .then(scene => {
        canvasScene = scene;
        setUpScene(scene);
    })
})

function fullWindow(elementNames=[]){
    elementNames.forEach(function(name){
        let element = document.querySelector('#'+name);
        if(!element) return;
        if(element.tagName === 'CANVAS'){
            element.height = window.innerHeight;
            element.width = window.innerWidth;
        }else{
            element.style.height = window.innerHeight + 'px';
            element.style.width = window.innerWidth + 'px';
        }
    })
}

window.addEventListener('load', () => {
    fullWindow(['webgl-canvas', 'ui-main']);
})

window.addEventListener('resize', () => {
    fullWindow(['ui-main']);
    if(canvasScene){
        canvasScene.resize(window.innerWidth, window.innerHeight);
    }
})